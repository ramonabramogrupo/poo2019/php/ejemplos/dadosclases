<?php

namespace Clase;

class Tirada {
    protected $dados=[];
    public $puntuacionTotal;
    
    public function __construct() {
        $this->jugar();
        $this->puntuacionTotal=$this->dados[0]->getValor()+$this->dados[1]->getValor();
    }
    
    private function jugar(){
        $this->dados[0]=new Dado(rand(1,6));
        $this->dados[1]=new Dado(rand(1,6));
    }
    
    public function render(){
        return "<div>" . $this->dados[0]->render() . $this->dados[1]->render() . "</div><div>Total:" . $this->puntuacionTotal . "</div>";
        
    }

    
}
