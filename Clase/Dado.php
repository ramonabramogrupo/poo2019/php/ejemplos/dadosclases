<?php

namespace Clase;

class Dado {
    protected $valor;
    protected $url;
    public function __construct($valor) {
        $this->valor = $valor;
        $this->crearUrl();
    }
    
    private function crearUrl(){
        $this->url="imgs/" . $this->valor . ".svg";
    }
    
    public function getValor() {
        return $this->valor;
    }
    
    public function render(){
        return '<img src="' . $this->url . '">';
    }



    
}
